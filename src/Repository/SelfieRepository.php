<?php

namespace App\Repository;

use App\Entity\Selfie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Selfie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Selfie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Selfie[]    findAll()
 * @method Selfie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SelfieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Selfie::class);
    }

    // /**
    //  * @return Selfie[] Returns an array of Selfie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Selfie
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
