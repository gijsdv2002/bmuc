<?php

namespace App\Controller;

use App\Entity\Selfie;
use App\Form\SelfieType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;


class SelfieController extends AbstractController
{

// handles and render the new selfie form page
    /**
     * @Route("/selfie/new", name="selfie_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $selfie = new Selfie();
        $form = $this->createForm(SelfieType::class, $selfie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photofile = $form->get('photo')->getData();

            $originalFilename = pathinfo($photofile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $photofile->guessExtension();
            $photofile->move(
                $this->getParameter('photo_directory'),
                $newFilename
            );
        $selfie->setPhoto( $this->getParameter('photo_directory').$newFilename);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($selfie);
        $entityManager->flush();
        return $this->redirectToRoute('view_selfie');
    }

        return $this->render('selfie/new.html.twig', [
            'selfie' => $selfie,
            'form' => $form->createView(),
        ]);
    }

//     handles de delete function for admin
    /**
     * @Route("/selfie/{id}", name="selfie_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Selfie $selfie): Response
    {
        if ($this->isCsrfTokenValid('delete' . $selfie->getId(), $request->request->get('_token'))) {

            $imgName = $selfie->getPhoto();
            $filesystem = new Filesystem();
            $filesystem->remove($imgName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($selfie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('view_selfie');
    }
}
