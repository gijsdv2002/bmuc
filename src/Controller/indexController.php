<?php


namespace App\Controller;


use App\Entity\Guestbook;
use App\Repository\GuestbookRepository;
use App\Repository\SelfieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class indexController extends AbstractController
{
// load index page
    /**
     *@Route("/", name="index")
     */
public function index(){

    return $this->render('index.html.twig');
}
// load and handles upload comments from guestbook
    /**
     *@Route("/guestbook", name="guestbook")
     */
    public function guestbook(GuestbookRepository $guestbookRepository):Response {

        if (isset($_POST['submit'], $_POST['comment'])) {
            $guestbookManager = $this->getDoctrine()->getManager();
            $guestbook = new Guestbook();

            $guestbook->setCommentary($_POST['comment']);
            $guestbookManager->persist($guestbook);
            $guestbookManager->flush();
        }




        return $this->render('guestbook.html.twig', [
            'guests' => $guestbookRepository->findAll(),
        ]);
    }
//    load impression page
    /**
     *@Route("/impression", name="impression")
     */
    public function impression(){

        return $this->render('impression.html.twig');
    }
//    load the selfie page
    /**
     *@Route("/selfie", name="view_selfie")
     */
    public function viewSelfie(SelfieRepository $selfieRepository){



        return $this->render('selfieview.html.twig',[
        'selfies' => $selfieRepository->findAll(),]);
    }
// load contact page
    /**
     *@Route("/contact-location", name="contact_location")
     */
    public function contactLocation(){



        return $this->render('contactLocation.html.twig');
    }
}