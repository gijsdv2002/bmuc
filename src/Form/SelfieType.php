<?php

namespace App\Form;

use App\Entity\Selfie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class SelfieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('photo', FileType::class,[
                'label' => ' ',
                'mapped'=>'false',
                'constraints'=>[
                    new File([
                        'maxSize' => '204800k',
                        'mimeTypes'=>[
                            'image/jpeg',
                            'image/png'
                        ]
                        ,
                        'mimeTypesMessage' => 'Please upload a valid img type',

                    ])
                ]
                    ]
            )
            ->add('commentary', TextareaType::class, ['required'=>true])
            ->add('name_person', TextType::class, ['required'=>true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Selfie::class,
        ]);
    }
}
